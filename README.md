# Ubuntu BlueZ with MIDI

The current BlueZ stack on Ubuntu does not support MIDI over bluetooth.
Modifying and building the package yourself with MIDI enabled is rather
simple. This repository contains the required patch and instructions on
how to apply it. It's not my intention to provide pre-built packages.

My main source of information is this thread:
<https://linuxmusicians.com/viewtopic.php?t=23610>

I'm aware of [these instructions
](https://linuxmusicians.com/viewtopic.php?t=23610) too. They are
valuable, but they don't use the Debian package management, resulting in
potential littering in your system. Package managers exist for a reason.
I prefer to patch and rebuild the Ubuntu/Debian package and have the
APT package manager install my modified package.

## Build and install Bluez with MIDI support on Ubuntu

### Prepare your system; get and patch the sources

Install the build essentials:

```sudo apt-get update && sudo apt-get install build-essential```

Clone this repository:

```git clone https://codeberg.org/plenae/ubuntu-bluez-with-midi.git```

Enter the repo dir and download the current source package:

```cd ubuntu-bluez-with-midi && apt-get source bluez```

Then add the libasound-dev build dependency in the package .dsc file and
`--enable-midi` to the debian/rules file.

[The patch included here](./ubuntu_bluez_with_midi_pkgsrc_5.60-0ubuntu2.2.patch)
shows you an example of what to change. Small differences will occur with
different libasound versions, but the change is basic enough.

```patch
diff -urNp orig/bluez-5.60/debian/rules modif/bluez-5.60/debian/rules
--- orig/bluez-5.60/debian/rules	2021-09-20 13:48:13.000000000 +0200
+++ modif/bluez-5.60/debian/rules	2022-03-13 10:00:45.743315350 +0100
@@ -15,6 +15,7 @@ CONFIGURE_FLAGS := \
 	--enable-datafiles \
 	--enable-debug \
 	--enable-library \
+	--enable-midi \
 	--enable-monitor \
 	--enable-udev \
 	--enable-obex \
diff -urNp orig/bluez_5.60-0ubuntu2.2.dsc modif/bluez_5.60-0ubuntu2.2.dsc
--- orig/bluez_5.60-0ubuntu2.2.dsc	2022-02-08 04:18:43.000000000 +0100
+++ modif/bluez_5.60-0ubuntu2.2.dsc	2022-03-13 09:59:42.037622151 +0100
@@ -13,7 +13,7 @@ Standards-Version: 3.9.6
 Vcs-Browser: https://git.launchpad.net/~bluetooth/bluez
 Vcs-Git: https://git.launchpad.net/~bluetooth/bluez
 Testsuite: autopkgtest
-Build-Depends: debhelper (>= 9.20160709), autotools-dev, dh-autoreconf, flex, bison, libdbus-glib-1-dev, libglib2.0-dev (>= 2.28), libcap-ng-dev, udev, libudev-dev, libreadline-dev, libical-dev, check (>= 0.9.8-1.1), systemd, libebook1.2-dev (>= 3.12), python3-docutils, libjson-c-dev
+Build-Depends: debhelper (>= 9.20160709), autotools-dev, dh-autoreconf, flex, bison, libdbus-glib-1-dev, libglib2.0-dev (>= 2.28), libcap-ng-dev, udev, libudev-dev, libreadline-dev, libical-dev, check (>= 0.9.8-1.1), systemd, libebook1.2-dev (>= 3.12), python3-docutils, libjson-c-dev, libasound2-dev
 Package-List:
  bluetooth deb admin optional arch=all
  bluez deb admin optional arch=linux-any
```

### Build and install

```bash
sudo apt-get -y build-dep bluez &&
sudo apt-get -y install libasound-dev &&
cd bluez-<version> &&
dpkg-buildpackage --no-sign &&
sudo dpkg -i bluez_<version_arch>.deb
```

Again, the versions will vary.

## Verify

You can check if your BlueZ stack supports MIDI using the ldd command. If the
output contains libasound.so, then your BlueZ stack support MIDI.

```bash
$ ldd /usr/sbin/bluetoothd 
        linux-vdso.so.1 (0x00007ffdccf32000)
        libglib-2.0.so.0 => /lib/x86_64-linux-gnu/libglib-2.0.so.0 (0x00007f815955c000)
        libdbus-1.so.3 => /lib/x86_64-linux-gnu/libdbus-1.so.3 (0x00007f815950e000)
        libasound.so.2 => /lib/x86_64-linux-gnu/libasound.so.2 (0x00007f8159415000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f81591ed000)
        libpcre.so.3 => /lib/x86_64-linux-gnu/libpcre.so.3 (0x00007f8159177000)
        libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f8159093000)
        libsystemd.so.0 => /lib/x86_64-linux-gnu/libsystemd.so.0 (0x00007f8158fd9000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f81597da000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f8158fd4000)
        liblzma.so.5 => /lib/x86_64-linux-gnu/liblzma.so.5 (0x00007f8158fa9000)
        libzstd.so.1 => /lib/x86_64-linux-gnu/libzstd.so.1 (0x00007f8158edc000)
        liblz4.so.1 => /lib/x86_64-linux-gnu/liblz4.so.1 (0x00007f8158ebc000)
        libcap.so.2 => /lib/x86_64-linux-gnu/libcap.so.2 (0x00007f8158eb1000)
        libgcrypt.so.20 => /lib/x86_64-linux-gnu/libgcrypt.so.20 (0x00007f8158d8c000)
        libgpg-error.so.0 => /lib/x86_64-linux-gnu/libgpg-error.so.0 (0x00007f8158d64000)
```
